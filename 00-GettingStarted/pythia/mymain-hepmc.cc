
#include "Pythia8/Pythia.h"
#include "Pythia8Plugins/HepMC2.h"
using namespace Pythia8;

//--------------------------------------------------------------------------

// Small helper function to get variation names.

string weightLabel(string weightString) {
  // Strip leading whitespace
  weightString.erase(0,weightString.find_first_not_of(" \t\n\r\f\v"));
  // Find first blank and use this to isolate weight label.
  int iBlank = weightString.find(" ", 0);
  return weightString.substr(0, iBlank);
}

//--------------------------------------------------------------------------

int main(int argc, char* argv[] ) {

  cout << "\n\n\tRunning PYTHIA with " << argc << " arguments.\n\n"; 

  Pythia pythia;
  pythia.readFile(argv[1]);
  pythia.init();

  // Interface for conversion from Pythia8::Event to HepMC one.
  HepMC::Pythia8ToHepMC ToHepMC;
  // Switch off warnings for parton-level events and do not store
  // cross section information, as this will be done manually.
  ToHepMC.set_print_inconsistency(false);
  ToHepMC.set_free_parton_exception(false);
  ToHepMC.set_store_pdf(false);
  ToHepMC.set_store_proc(false);
  ToHepMC.set_store_xsec(false);
  // Vector of output HepMC event files.
  vector< HepMC::IO_GenEvent* > ev;
  HepMC::IO_GenEvent eve("multiweight.hepmc", std::ios::out);

  // Create multiple output HepMC files, one for each variation.
  // Also, create one cross section container for each event weight.
  vector<double> sigmaTot;
  vector<string> weightStrings = pythia.settings.wvec("UncertaintyBands:List");
  for (int iwg=0; iwg < pythia.info.nVariationGroups(); ++iwg) { 
    string filename = (iwg==0)
      ? "baseline.hepmc" : pythia.info.getGroupName(iwg) + ".hepmc";
    ev.push_back( new HepMC::IO_GenEvent(filename, std::ios::out));
    sigmaTot.push_back(0.);
  }

  Hist pT  ("top transverse momentum",       100,  0., 200.);
  Hist eta ("top pseudorapidity",            100, -5., 5.);
  Hist nCh ("charged particle multiplicity", 100, -1., 399.);

  int nEvent = pythia.mode("Main:numberOfEvents");
  for (int iEvent=0; iEvent < nEvent; ++iEvent) {

    pythia.next();

    int iTop = 0;
    int nChg = 0;
    for (int i=0; i < pythia.event.size(); ++i) {
      if (pythia.event[i].id() == 6) iTop = i;
      if (pythia.event[i].isFinal() && pythia.event[i].isCharged()) ++nChg;
    }
    pT.fill(pythia.event[iTop].pT());
    eta.fill(pythia.event[iTop].eta());
    nCh.fill( nChg );

    // Write one HepMC event file for each variation.
    for (int iwg = 0; iwg < pythia.info.nVariationGroups(); ++iwg) {
      HepMC::GenEvent* hepmcevt = new HepMC::GenEvent();
      // Set event weight
      double w = pythia.info.getGroupWeight(iwg);
      // Add the weight of the current event to the cross section.
      sigmaTot[iwg]  += w;
      hepmcevt->weights().push_back(w);
      // Fill HepMC event
      ToHepMC.fill_next_event( pythia, hepmcevt );
      // Report cross section to hepmc
      HepMC::GenCrossSection xsec;
      //xsec.set_cross_section( sigmaTot[iwg]*1e9,
      xsec.set_cross_section( sigmaTot[0]*1e9,
        pythia.info.sigmaErr()*1e9 );
      hepmcevt->set_cross_section( xsec );
      // Write the HepMC event to file. Done with it.
      *ev[iwg] << hepmcevt;
      delete hepmcevt;
    }

    // Write a multi-weight HepMC event file.
    HepMC::GenEvent* hepmcevt = new HepMC::GenEvent();
    for (int iwg = 0; iwg < pythia.info.nVariationGroups(); ++iwg) {
      // Set event weight
      double w = pythia.info.getGroupWeight(iwg);
      // Add the weight of the current event to the cross section.
      sigmaTot[iwg]  += w;
      // Write weights with names. Challenge: Make this work for Rivet 3!
      //hepmcevt->weights()[pythia.info.getGroupName(iwg)] = w;
      // Write weights in numerical order.
      hepmcevt->weights()[std::to_string(iwg)] = w;
    }
    // Fill HepMC event
    ToHepMC.fill_next_event( pythia, hepmcevt );
    // Report cross section to hepmc
    HepMC::GenCrossSection xsec;
    //xsec.set_cross_section( sigmaTot[iwg]*1e9,
    xsec.set_cross_section( sigmaTot[0]*1e9,
      pythia.info.sigmaErr()*1e9 );
    hepmcevt->set_cross_section( xsec );
    // Write the HepMC event to file. Done with it.
    eve << hepmcevt;
    delete hepmcevt;

  }

  cout << pT << eta << nCh;

  pythia.stat();

  return 0;

}
