#!/usr/bin/env bash

rivet --skip-weights --pwd -a MY_ANALYSIS:INVMODE=STRICT -H output_inv_strict.yoda myWjetsEvents10k.hepmc2g
rivet --skip-weights --pwd -a MY_ANALYSIS:INVMODE=DETECT -H output_inv_detect.yoda myWjetsEvents10k.hepmc2g
