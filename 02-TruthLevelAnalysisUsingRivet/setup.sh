#!/usr/bin/env bash

# use Python version from Docker image
alias python='docker run -i --rm  -u `id -u $USER`:`id -g`  -v $PWD:$PWD -w $PWD  hepstore/rivet-sherpa:3.0.1-2.2.7 python'

# Rivet tools ...
alias rivet='docker run -i  --rm  -u `id -u $USER`:`id -g`  -v $PWD:$PWD -w $PWD  hepstore/rivet-sherpa:3.0.1-2.2.7 rivet'
alias rivet-buildplugin='docker run -i  --rm  -u `id -u $USER`:`id -g`  -v $PWD:$PWD -w $PWD  hepstore/rivet-sherpa:3.0.1-2.2.7 rivet-buildplugin'
alias rivet-cmphistos='docker run -i  --rm  -u `id -u $USER`:`id -g`  -v $PWD:$PWD -w $PWD  hepstore/rivet-sherpa:3.0.1-2.2.7 rivet-cmphistos'
alias rivet-mkanalysis='docker run -i  --rm  -u `id -u $USER`:`id -g`  -v $PWD:$PWD -w $PWD  hepstore/rivet-sherpa:3.0.1-2.2.7 rivet-mkanalysis'
alias rivet-mkhtml='docker run -i  --rm  -u `id -u $USER`:`id -g`  -v $PWD:$PWD -w $PWD  hepstore/rivet-sherpa:3.0.1-2.2.7 rivet-mkhtml'
alias make-plots='docker run -i  --rm  -u `id -u $USER`:`id -g`  -v $PWD:$PWD -w $PWD  hepstore/rivet-sherpa:3.0.1-2.2.7 make-plots'

# YODA tools ...
alias yodamerge='docker run -i --rm  -u `id -u $USER`:`id -g`  -v $PWD:$PWD -w $PWD  hepstore/rivet-sherpa:3.0.1-2.2.7 yodamerge'
alias yodascale='docker run -i --rm  -u `id -u $USER`:`id -g`  -v $PWD:$PWD -w $PWD  hepstore/rivet-sherpa:3.0.1-2.2.7 yodascale'
alias yodadiff='docker run -i --rm  -u `id -u $USER`:`id -g`  -v $PWD:$PWD -w $PWD  hepstore/rivet-sherpa:3.0.1-2.2.7 yodadiff'
alias yoda2flat='docker run -i --rm  -u `id -u $USER`:`id -g`  -v $PWD:$PWD -w $PWD  hepstore/rivet-sherpa:3.0.1-2.2.7 yoda2flat'
alias yodals='docker run -i --rm  -u `id -u $USER`:`id -g`  -v $PWD:$PWD -w $PWD  hepstore/rivet-sherpa:3.0.1-2.2.7 yodals'

# Sherpa tools ...
alias Sherpa='docker run -i --rm  -u `id -u $USER`:`id -g`  -v $PWD:$PWD -w $PWD  hepstore/rivet-sherpa:3.0.1-2.2.7 Sherpa'

