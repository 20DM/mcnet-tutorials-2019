# Advanced material: Rivet and Contur

### Environment Setup

A docker image has been setup for this part of the tutorial. You can pull the image by running:

         $ docker pull huangjoanna/contur

You may need to repeat the pull to get the most updated version. You should now see this docker image listed by running this command:

         $ docker images

#### Running contur through docker

Once you have pulled the docker image, make a directory in your filesystem, go into it and start an interactive docker session:

         $ mkdir ~/mcnet/runarea
         $ cd runarea
         $ docker run -it -v $PWD:$PWD -w $PWD --rm huangjoanna/contur bash

The `-v` and `-w` arguments mount your current directory on the host system into the container and make it the current directory inside the container as well.

Inside your docker interactive session, the $CONTURMODULEDIR environment variable points to the root of your contur installation. 
Now you have a working installation, get ready to run...

#### Choose and setting up a Model

This section is specific to running Herwig with a UFO model, a recommended first step.
Skip if you just want to run on an existing HepMC file.

First copy over the GridPack area:

        $ cp -r  $CONTURMODULEDIR/AnalysisTools/GridSetup/GridPack .

- Choose a model. You should copy a UFO model directory from somewhere. Some of those previously used
in contur are in '$HOME/contur/Models'. Start with the DM_vector_mediator_UFO model, which is the one used in the first contur paper.

        $ cd GridPack
        $ cp -r $CONTURMODULEDIR/Models/DM/DM_vector_mediator_UFO .

- Build the UFO model using Herwig's 'ufo2herwig' command.

        $ ufo2herwig DM_vector_mediator_UFO/
        $ make
	$ cd ..

#### Running Herwig

Herwig reads an input command file, e.g. LHC.in, to define a run.

In this tutorial we have a version of Herwig built without the rivet interface, so we will be writing Herwig events to a file
or a unix pipe and reading them from there into a seperate rivet process. There is an example file in the model directory to do this.
Copy this example `LHC-pipe-test.in` file from inside the model to the top level of your run area:
  
        $ cp GridPack/DM_vector_mediator_UFO/LHC-pipe-test.in LHC.in

- Build the Herwig run card (LHC.run).

        $ Herwig read LHC.in -I GridPack -L GridPack

- Run the Herwig run card, specifying the number of events to generate. This
  can take a while so, as a first test, running around 200 events is fine.

        $ Herwig run LHC.run -N 200

- This will produce the file LHC.hepmc containing the events generated by Herwig

#### Running rivet standalone on a HepMC file 

All that is needed is 

        $ rivet -a $RA13TeV LHC.hepmc -o LHC.yoda

where the argument following the `-a` is a comma-separated list of the analyses you want to run. 
Contur defines some convenient environment variables with the list of relevant analyses for 7, 8 and 13 TeV LHC running, $RA7TeV, $RA8TeV and $RA13TeV. 
Rivet will produce a .yoda file which you can then use in the following steps. For other rivet command line options, see

        $ rivet --help

as usual.

If you are running a generator independently of Rivet, you can use a pipe for this, so the (sometimes large) 
file never has to be resident on disk. For example (if you have pythia installed already):

        $ mkfifo fifo.hepmc
	$ run-pythia -n 20000 -e 8000 -c Top:all=on -o fifo.hepmc &
	$ rivet fifo.hepmc -a $RA8TeV

#### Running Contur on a single yoda file.

Once you have produced a yoda file from rivet, this step is applicable however you produced it.

- Analyse a Yoda file created by one of the procedures above with contur. (Run 'contur --help' to see more options).

        $ contur -f LHC.yoda 

- The contur script will output a plots folder and an ANALYSIS folder and
  print some other information. (The `-f` flag just speeds things up a bit for single file running.) 

- Generate 'contur-plots' directory containing histograms and an index.html
  file to view them all. Whilst still in the GridPack directory, run:

        $ contur-mkhtml LHC.yoda

  This will have created a `contur-plots` directory contain plots and an html index file. View this file in
  a browser (from outside of your docker session). It will have a summary of the run at the top, a list of
  the measurements which give the best discrimmination against your model, and then a detailed listing of the
  comparison to each rivet analysis/paper tested.

#### Parameter scan and heatmap

The previous instructions tested a single parameter point. You can change the parameters in LHC.in by hand and rerun to
test others. Contur can also auto-generate a scan, which can be submitted to a batch farm. We don't have a batch farm available
so will do a small grid by hand to demonstrate the machinery.

- In your run area, define a parameter space in 'param_file.dat'. Again, there is an example in the model directory, so
copy this:

         $ cp GridPack/DM_vector_mediator_UFO/param_file.dat .

This file is formatted like this:

```

#Based off configObj python template
[Run]
generator ='[path to my Herwig installation]/setupEnv.sh'
contur ='[path to my contur installation]/contur/setupContur.sh'

[Parameters]
[[mXm]]
start = 300.0
stop = 500.0
mode = LOG
number = 15
[[mY1]]
start = 200.0
stop =  500.0
mode = LIN
number=30
[[gYq]]
mode=CONST
value=0.1
[[gYXm]]
mode=REL
form=3.0*{gYq}/{Y1}
```

- Normally, `setupEnv.sh` should be a script which sets up your runtime environment which the batch jobs will use. As a minimum
   is will need to contain the lines ro source your rivetenv.sh and yodaenv.sh files. Similarly `setupContur.sh` is the contur 
   setup script in your contur directory. However, since we are not submitting batch jobs, we don't need these lines, so comment
   them out.

- We need a slightly modified LHC.in file, since the batch submission adds the beam information itself. So copy this file (move your previous LHC.in file somewhere else if you want to saave it):

        $ cp GridPack/DM_vector_mediator_UFO/LHC-example.in LHC.in

- You should check that the parameters defined are also defined at the top of
  the LHC.in file within the same directory.

- The example model has parameters 'Xm', 'Y1', 'gYXm', 'gYq', defined in
  'params_file.dat' and the LHC-example.in file has, at the top of the file:

        read FRModel.model
        set /Herwig/FRModel/Particles/Y1:NominalMass {mY1}
        set /Herwig/FRModel/Particles/Xm:NominalMass {mXm}
        set /Herwig/FRModel/FRModel:gYXm {gYXm}
        set /Herwig/FRModel/FRModel:gYq  {gYq}	 

  If you wanted to add or remove parameters you must do this in both files. For other models, different
  parameters are available/required.
 
- Edit params_file.dat so the `number` variables are both set to 3. This will generate a small (3x3) grid for
  each LHC beam energy.
 
- Run a test scan over the parameter space defined in 'param_file.dat' without submitting it
  to a batch farm.  (The `-s` flag ensure no jobs will be submitted.)

         $ batch-submit -n 200 --seed 101 -s -P

  (The -P is needed because our version of Herwig does not have the native rivet interface installed, so we have to run hepmc events
  through a pipe.)

  This will produce a directory called 'myscan00' containing one directory for each known beam energy, each containing
  however many runpoint directories are indicated by the ranges in your param_file.dat. Have a look at the shell scripts 
  ('runpoint_xxxx.sh') which have been generated and the LHC.in files to check all is as you expected. 
  You can manually submit some of the 'runpoint_xxxx.sh' files as a test, or run Herwig local as above using the generated 
  LHC.in files.

- Now we would be ready to run batch jobs. To do so we'd remove the myscan00 directory tree we just created, and run the 
  batch submit command again, now without the `-s` flag and specifying the queue
  on your batch farm. For example:

         $ batch-submit -n 1000 --seed 101 -P -q mediumc7
  
  (Note that we assume `qsub` is available on your system. If you have a different submission system you'll need to
  look into `$HOME/contur/AnalysisTools/contur/bin/batch-submit` and work out how to change the appropriate submission
  commands.)
  A successful run will produce a directory called 'myscan00' as before. You need to wait for the farm
  to finish the jobs before continuing. You can check the progress using the 'qstat' command.

- However, since we don't think a batch system will be available for the tutorial, we suggest running each script by hand:

        $ cd myscan00/7TeV/0000/
        $ source runpoint_0000.sh 

  etc.

- When these nine runs for each beam energy, or the batch job, is complete there should, in every run point directory, be
  files 'LHC-runpoint_xxx.yoda'.

- Analyse results with contur. The resulting .map file will be output to the ANALYSIS folder.

        $ contur -g msycan00/

  For various options see

        $ contur --help

- Plot a heatmap.

        $ cd ANALYSIS/
        $ contur-plot contur.map mXm mY1 -T "My First Heatmap"


See the [Contur README file](https://bitbucket.org/heprivet/contur/src/rivet3-correlations/README.md) for general instructions 
(repeating some of these). 

#### Additional

A couple of models have been installed with the tutorial materials. 
Others are available [here](https://bitbucket.org/heprivet/contur/src/default/Models/) 
with example steering files, or you can get your own favourite from somewhere, 
e.g. the [Feynrules Model Database](https://feynrules.irmp.ucl.ac.be/wiki/ModelDatabaseMainPage)

Example Herwig `LHC-example.in` files are provided in the model directories. These
usually specify parameters in curly brackets, e.g. `{name}`, for use
with the batch system. For a first single run, replace these expressions with some
concrete numerical value. 
(This test file also specifies beam energies and saves the configuration. For batch running, those lines
are added automatically by the submission script.)



