source /usr/local/rivetenv.sh;
source /contur/setupContur.sh;
cd /Users/jhuang/mcnet/runarea2/partB/myscan00/13TeV/0004;
Herwig read LHC.in -I /Users/jhuang/mcnet/runarea2/partB/GridPack -L /Users/jhuang/mcnet/runarea2/partB/GridPack;
mkfifo LHC-S101-runpoint_0004.hepmc; 
Herwig run LHC.run --seed=101 --tag=runpoint_0004 --numevents=200&
rivet -a $RA13TeV -n 200 -o LHC-S101-runpoint_0004.yoda LHC-S101-runpoint_0004.hepmc 
