# 
# Limit file for NeutrinoEFT
# Should all return 1 if excluded, 0 otherwise.

# in GeV/s
hbar = 6.58e-25

def HBR(paramDict):  
    """ ATLAS limit on BSM Higgs decay BF. return 1 if  excluded, 0 otherwise """
    import numpy as np

    hbr=float(paramDict["H->N1"])

    if hbr > 0.24: 
        return 1.0
    else:
        return 0.0

def Nlife(paramDict):  
    """ return 1 if  excluded, 0 otherwise
        Reject if the heavy neutrino decays noon-prompt but in the detector volume """
    import numpy as np

    nwid=float(paramDict["Parent: N1"])
    # in picoseconds
    nlifeps = 1e12 * hbar / nwid

    print nlifeps
    if nlifeps < 10: 
        return 0.0

    if nlifeps > 3000:
        return 0.0

    return 1.0
