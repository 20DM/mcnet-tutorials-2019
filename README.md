# 13th MCnet Summer School Tutorial

## Environment Setup

The tutorials are based on Docker.
If you are using MacOS or Windows, you will first need to create a DockerID at https://hub.docker.com/signup

Head to https://docs.docker.com/install for installation instructions.

You can check that Docker has installed properly by running the `hello-world` Docker image

        $ docker run hello-world

Some helpful commands:

`docker run [OPTIONS] IMAGE` to run an image; 
`docker ps` to list active containers; 
`docker image ls` to list available images/apps; 
`docker attach [OPTIONS] CONTAINER` to attach to a container

When you are running inside a container, you can use `CTLR-p CTLR-q` to detach from it and leave it running. 

## Check Out Repository

Check out code from the repository by running:

        $ git clone https://gitlab.com/20DM/mcnet-tutorials-2019.git

Now a directory called mcnet-tutorial-material should have been created.

To get the most recent version, first go into the directory, and run the following:

        $ cd mcnet-tutorials-2019/
        $ git pull


